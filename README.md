# Boron Hydride

This is a template for users of the [inq code](https://gitlab.com/npneq/inq).
It is a quick way to download inq and compile your runs.

## Downloading inq_template

The first step is to download the template code (you do not need inq installed before doing this).
You have two options.
You can download and decompress a tarball (using wget or a similar command) like this:

```shell
$ wget https://gitlab.com/npneq/inq_template/-/archive/main/inq_template-main.tar.gz
$ tar -xvzf inq_template-main.tar.gz
$ cd inq_template-main
```
or you can use git to download the directory

```shell
$ git clone https://gitlab.com/npneq/inq_template.git
$ cd inq_template
```

They are equivalent for most users. But the second one will keep the files in a git repository you can use to track your changes if you want.

## Configure and compile

Once you downloaded the code, we know need to configure it to download and compile inq (this will be done automatically behind the scenes).
inq_template uses the cmake build system for the configuration.
The first step is to create a directory where we will compile and we will install.

```shell
mkdir build/
mkdir install/
cd build/
```

Now we run the configure step.
Here we might need to specify some details for the machine we are using.
We will assume for the moment that no options are needed in this machine.

```shell
$ cmake .. -DCMAKE_INSTALL_PREFIX=`pwd`/../install -DCMAKE_BUILD_TYPE=Release 
```

This might take a few minutes as here the source codes for inq and other required libraries are downloaded.
Note that we told cmake to install inq files in the ../install/ directory we created earlier.
If the configuration run successfully we can now compile with

```
$ make -j4
$ make install
```

(feel free to replace 4 with the number of processor cores available in your system).
This might take several minutes.

## Run program

Once compiled we can run the example file to check that everything is working fine.
The example is called nitrogen.cpp and it is located in the example/ directory.
To run it, simply use:cd 

```shell
$ cd runs
$ ./nitrogen
```

This should only take a few seconds to run.

